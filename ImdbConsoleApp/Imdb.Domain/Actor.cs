﻿using System;
using System.Collections.Generic;

namespace Imdb.Domain
{

    public class Actor
    {
        public Actor()
        {

        }

        public int ActorId { get; set; }
        public string ActorName { get; set; }
        public DateTime ActorDob { get; set; }

        public  Actor(int actorid, string name, DateTime dob)
        {
            ActorId = actorid;
            ActorName = name;
            ActorDob = dob;
        }

    }
}

