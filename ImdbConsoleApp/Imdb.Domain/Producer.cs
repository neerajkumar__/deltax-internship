﻿using System;
using System.Collections.Generic;

namespace Imdb.Domain
{

    public class Producer
    {
        public Producer()
        {

        }

        public int  ProducerId {get; set;}
        public string ProducerName { get; set; }
        public DateTime ProducerDob { get; set; }


        public  Producer(int producerid, string name, DateTime dob)
        {
            ProducerId = producerid;
            ProducerName = name;
            ProducerDob = dob;
        }

    }
}
