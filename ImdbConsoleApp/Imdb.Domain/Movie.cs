﻿using System;
using System.Collections.Generic;

namespace Imdb.Domain
{
    public class Movie
    {
        public Movie()
        {

        }

        public int MovieId { get; set; }
        public string MovieName { get; set; }
        

        public List<int> ActorId { get; set; }

        public int ProducerId { get; set; }

        public string Plot { get; set; }

        public string Year { get; set; }

        public Movie(int movieid,string moviename, string year, string plot,  List<int>actorId, int producerId)
        {
            MovieId = movieid;
            MovieName = moviename;
            Year = year;
            Plot = plot;
            ActorId = actorId;
            ProducerId = producerId;
   

        }



    }
}

 