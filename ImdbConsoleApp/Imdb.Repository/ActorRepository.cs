﻿using Imdb.Domain;
using System;
using System.Collections.Generic;
using System.Text;


namespace Imdb.Repository
{
    public class ActorRepository
    {
        
  
        private List<Actor> _actors;


        public ActorRepository()
        {
    
            _actors = new List<Actor>();
     
        }

    
      

        public void AddActor(Actor actor)
        {
            _actors.Add(actor);
        }

     


        public List<Actor> GetActors()
        {
            return _actors;
        }


    }
}

