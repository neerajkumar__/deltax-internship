﻿using Imdb.Domain;
using System;
using System.Collections.Generic;
using System.Text;


namespace Imdb.Repository
{
    public class ProducerRepository
    {


        private List<Producer> _producers;


        public ProducerRepository()
        {

            _producers = new List<Producer>();

        }


        public void AddProducer(Producer producer)
        {
            _producers.Add(producer);
        }



        public List<Producer> GetProducers()
        {
            return _producers;
        }
    }
}

