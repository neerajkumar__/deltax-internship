﻿using Imdb.Domain;
using System;
using System.Collections.Generic;
using System.Text;


namespace Imdb.Repository
{
    public class MovieRepository
    {


        private List<Movie> _movies;


        public MovieRepository()
        {

            _movies = new List<Movie>();

        }

        public void AddMovie(Movie movie)
        {
            _movies.Add(movie);
        }


        public List<Movie> GetMovies()
        {
            return _movies;
        }

        public void Delete(int movieid)
        {
            _movies.RemoveAll(x => x.MovieId == movieid);
        }
    }
}

