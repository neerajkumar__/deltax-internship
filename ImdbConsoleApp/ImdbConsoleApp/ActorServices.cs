﻿using Imdb.Domain;
using Imdb.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImdbConsoleApp
{
     public class ActorServices
     {

        
        private readonly ActorRepository _actorrepository;
        public ActorServices()
        {
           _actorrepository = new ActorRepository();
        }

             
        
        public void AddActor(int actorid, string actorName, DateTime actorDob)
        {
            if (string.IsNullOrEmpty(actorName))
            {
                throw new ArgumentException("Invalid arguments");
            }

            var actors = new Actor()
            {
                ActorId=actorid,
                ActorName = actorName,
                ActorDob = actorDob
            };

            _actorrepository.AddActor(actors);


        }

       
        public List<Actor> GetActors()
        {
            return _actorrepository.GetActors();
        }

       

        




    }
}
