﻿using Imdb.Domain;
using Imdb.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImdbConsoleApp
{
    class ProducerServices
    {



        private readonly ProducerRepository _producerrepository;
        public ProducerServices()
        {
            _producerrepository = new ProducerRepository();
         }

         public void AddProducer(int producerId, string producerName, DateTime producerDob)
         {
            if (string.IsNullOrEmpty(producerName)) 
            {
                throw new ArgumentException("Invalid arguments");
            }

            var producer1 = new Producer()
            {
                ProducerId = producerId,
                ProducerName = producerName,
                ProducerDob = producerDob
            };

            _producerrepository.AddProducer(producer1);


        }

        public List<Producer> GetProducers()
        {
            return _producerrepository.GetProducers();
        }

       
    }
}
