﻿using System;
using Imdb.Domain;
using Imdb.Repository;
using System.Collections.Generic;
using System.Text;

namespace ImdbConsoleApp
{
    public class MovieServices
    {
        
        private readonly MovieRepository _movierepository;
        public MovieServices()
        {
            _movierepository = new MovieRepository();
        }



        public void AddMovie(int movieid,string moviename, string year,  string plot,  List<int> actorId, int producerId)
        {
            if (string.IsNullOrEmpty(moviename)) 
            {
                throw new ArgumentException("Invalid arguments");
            }

            var movies = new Movie()
            {
                MovieId=movieid,
                MovieName = moviename,
                Plot = plot,
                Year = year,
                ActorId = actorId,
                ProducerId = producerId
            };

            _movierepository.AddMovie(movies);
        }

      
     

        public List<Movie> GetMovies()
        {
            return _movierepository.GetMovies();
        }

        public void DeleteMovie(int movieid)
        {
            _movierepository.Delete(movieid);
        }

       
        
    }
}
