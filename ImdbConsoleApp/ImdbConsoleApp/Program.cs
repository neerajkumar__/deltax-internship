﻿using System;
using Imdb.Domain;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ImdbConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            ActorServices actorService = new ActorServices();

             MovieServices movieService = new MovieServices();

            ProducerServices producerService = new ProducerServices();

            //List<Movie> movielist = new List<Movie>();
            //List<Actor> actorlist = new List<Actor>();
            //List<Producer> producerlist = new List<Producer>();

            int actorid = 1;
            int producerid = 1;
            int movieid = 1;

            while (true)
            {

                Console.WriteLine("\n ------------------------------- ");
                Console.WriteLine("  welcome to imdb console app  ");

                Console.WriteLine(" ------------------------------- ");

                Console.WriteLine(" --------- MENU ---------------- ");
                Console.WriteLine(" 1. List Movies ");
                Console.WriteLine(" 2. Add Movie ");
                Console.WriteLine(" 3. Add Actor ");
                Console.WriteLine(" 4. Add Producer ");
                Console.WriteLine(" 5. Delete Movie ");
                Console.WriteLine(" 6. Exit ");

                Console.WriteLine("\nPlease select a option from menu : ");
                string Options = Console.ReadLine();

                switch (Options)
                {

                    case "1":

                        // list of movies
                        
                        foreach( var samplemovie in movieService.GetMovies())
                        {
                            Console.WriteLine("MOVIE NAME :" + samplemovie.MovieName);
                            Console.WriteLine("MOVIE YEAR :" + samplemovie.Year);
                            Console.WriteLine("MOVIE PLOT :" + samplemovie.Plot);

                            var actornamebyId = (from s in actorService.GetActors()
                                                 where samplemovie.ActorId.Contains(s.ActorId)
                                                 select s.ActorName).ToList();

                            Console.WriteLine("MOVIE ACTORS:");
                            System.Console.WriteLine(string.Join(", ", actornamebyId));
                            //foreach(var x in actornamebyId)
                            //{
                            //    Console.WriteLine(x);
                            //}



                            Console.WriteLine("MOVIE PRODUCER:");
                            foreach (var pid in producerService.GetProducers())
                            {
                              if (pid.ProducerId==samplemovie.ProducerId)
                                {
                                    Console.WriteLine(pid.ProducerName);
                                    break;
                                }
                            }
                           

                        }


                        break;


                    case "2":

                        Console.WriteLine(" Enter Movie Name: ");
                        string movieName = Console.ReadLine();

                        Console.Write("Enter Movie Year: ");
                        string movieYear = Console.ReadLine();

                        Console.Write("Enter Plot: ");
                        string plot = Console.ReadLine();


                        //we have to give list of actors with ids and actor name
                        Console.WriteLine("Choose Actors by their Id's");
                        foreach (var i in actorService.GetActors())
                        {
                            Console.WriteLine(i.ActorId + " " + i.ActorName);
                        }

                        List<int> actorID = new List<int>();
                        Console.WriteLine("select the actor id's");

                        string[] actorSelect = Console.ReadLine().Split(" ");
                        foreach (var i in actorSelect)
                        {
                            actorID.Add(int.Parse(i));
                        }

                        //we have to give list of producer with ids and producer name
                        Console.WriteLine("Choose Producer by their Id's");
                        foreach (var i in producerService.GetProducers())
                        {
                            Console.WriteLine(i.ProducerId + " " + i.ProducerName);
                            
                        }

                        int producerID = int.Parse(Console.ReadLine());

                        //var movie = new Movie(movieName, movieYear, plot, actorID, producerID);  //creating Movie


                        //add to the movie list

                        movieService.AddMovie(movieid++,movieName, movieYear, plot, actorID, producerID);

                   

                        break;


                    case "3":

                        Console.WriteLine(" Enter Actor Name: ");
                        string actorName = Console.ReadLine();

                        Console.Write("Enter Actor DOB: ");
                        DateTime actorDob = DateTime.Parse(Console.ReadLine());


                        //var actor = new Actor(actorid++,actorName, actorDob);
                        //actorlist.Add(actor);   //add actor to the list

                        //actorid++;

                        actorService.AddActor(actorid++, actorName, actorDob);



                        break;

                    case "4":
                        Console.WriteLine(" Enter Producer Name: ");
                        string producerName = Console.ReadLine();

                        Console.Write("Enter Producer DOB: ");
                        DateTime producerDob = DateTime.Parse(Console.ReadLine());


                        //var producer = new Producer(producerid,producerName, producerDob);  
                        //producerlist.Add(producer);   //adding producer to the list

                        producerService.AddProducer(producerid++,producerName,producerDob);

                        //producerid++;

                        break;


                    case "5":

                        // here, returning the movie names
                        foreach (var i in movieService.GetMovies()) Console.WriteLine(i.MovieId+" "+i.MovieName);

                        Console.WriteLine("enter the movie to  be deleted");
                        int deletemovie = Convert.ToInt32(Console.ReadLine());

                        //deleting the movie name
                        movieService.DeleteMovie(deletemovie);


                        break;


                    case "6":
                        Environment.Exit(-1);

                        break;

                    default:

                        Console.WriteLine("Please enter valid option!!");

                        break;

                }
            }
        }
    }
}