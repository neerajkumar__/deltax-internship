﻿using ImdbConsoleApp;
using Imdb.Domain;
using System.Linq;
using System.Collections.Generic;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Imdb.Tests
{
    [Binding]
    public class ImdbFeatureSteps
    {
        private MovieServices _movieService;
        private ActorServices _actorService;

        public ImdbFeatureSteps()
        {
            _movieService = new MovieServices();
            _actorService = new ActorServices();
        }
        private string _moviename, _year, _plot;
        public int _movieid, _producerid;
        private Exception _exception;
        private List<Movie> _movies = new List<Movie>();
        public List<int> _actorid = new List<int>();


        [Given(@"I have a movie id ""(.*)""")]
        public void GivenIHaveAMovieId(int movieid)
        {
            _movieid = movieid;
        }
        
        [Given(@"movie name ""(.*)""")]
        public void GivenMovieName(string moviename)
        {
            _moviename = moviename;
        }
        
        [Given(@"release year ""(.*)""")]
        public void GivenReleaseYear(string year)
        {
            _year = year;
        }
        
        [Given(@"Plot ""(.*)""")]
        public void GivenPlot(string plot)
        {
            _plot = plot;
        }
        
        [Given(@"ActorID ""(.*)""")]
        public void GivenActorID(string actorid)
        {
            string[] actorSelect = actorid.Split(",");
            foreach (var i in actorSelect)
            {
                _actorid.Add(int.Parse(i));
            }
        }
        
        [Given(@"ProducerID ""(.*)""")]
        public void GivenProducerID(int producerid)
        {
            _producerid = producerid;
        }
        
        [Given(@"I have a library of movies")]
        public void GivenIHaveALibraryOfMovies()
        {
            _movies = _movieService.GetMovies();
        }
        
        [When(@"I add the movie to  the IMDB")]
        public void WhenIAddTheMovieToTheIMDB()
        {
            try
            {
                _movieService.AddMovie(_movieid, _moviename, _year, _plot, _actorid, _producerid);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }
        
        [When(@"I fetch my movies")]
        public void WhenIFetchMyMovies()
        {
            _movies = _movieService.GetMovies();
        }
        
        [Then(@"add the movie to IMDB")]
        public void ThenAddTheMovieToIMDB(Table table)
        {
            var movies = _movieService.GetMovies();
            table.CompareToSet(movies);
        }
        
        [Then(@"I should have the following movies")]
        public void ThenIShouldHaveTheFollowingMovies(Table table)
        {
            var movies = _movieService.GetMovies();
            table.CompareToSet(_movies);
        }
    }
}
