﻿Feature: ImdbFeature
		Adding a Movie and listing the Movie

@addMovie
Scenario: Add a Movie to IMDB App
	Given I have a movie id "1"
	And  movie name "3 idiots"
	And release year "2009"
	And Plot " 3 friends journey"
	And ActorID "1,2"
	And ProducerID "1"
	When I add the movie to  the IMDB
	Then add the movie to IMDB
	| movieid | moviename   | year   | plot               | actorid   | producerid |
	|1        | 3 idiots    | 2009   | 3 friends journey  | 1,2       | 1          |
	
	


@listMovie
Scenario: List all movies from IMDB
	Given I have a library of movies
	When I fetch my movies
	Then I should have the following movies
	| movieid | moviename     | year | plot               | actorid | producerid |
	| 1       | 3 friends     | 2009 | 3 friends journey  | 1       | 1          |
	| 2       | Vikram Vedha  | 2017 | police chase       | 2       | 2          |



 