
DROP DATABASE IF EXISTS IMDB;

CREATE DATABASE IMDB;
USE IMDB;

DROP TABLE IF EXISTS Actors;

CREATE TABLE Actors(
Id INT PRIMARY KEY IDENTITY(1,1),
Name VARCHAR(MAX),
Gender VARCHAR(MAX),
DOB DATE,
Bio VARCHAR(MAX)
);

INSERT INTO Actors VALUES('Mila Kunis','Female','1986/11/14','hey its Mila');
INSERT INTO Actors VALUES('Robert DeNiro','Male','1957/07/10','hey its Robert');
INSERT INTO Actors VALUES('George Michael','Male','1978/11/23','hey its George');
INSERT INTO Actors VALUES('Mike Scott','Male','1969/08/06','hey its Mike');
INSERT INTO Actors VALUES('Pam Halpert','Female','1996/09/26','hey its Pam');
INSERT INTO Actors VALUES('Dame Judi Dench','Female','1947/04/05','hey its Dame');

SELECT * 
FROM Actors;

DROP TABLE IF EXISTS Producers;
	
CREATE TABLE Producers(
Id INT PRIMARY KEY IDENTITY(1,1),
Name VARCHAR(MAX),
Gender VARCHAR(MAX), 
DOB DATE,
Bio VARCHAR(MAX)
);

INSERT INTO Producers VALUES('Arjun','Fox','1998/05/14','hey its Arjun');
INSERT INTO Producers VALUES('Arun','Bull','2004/09/11','hey its Arun');
INSERT INTO Producers VALUES('Tom','Hanks','1987/11/03','hey its Tom');
INSERT INTO Producers VALUES('Zeshan','Male','1996/11/14','hey its Zeshan');
INSERT INTO Producers VALUES('Nicole','Male','1992/09/26','hey its Nicole');

SELECT * 
FROM Producers;

DROP TABLE IF EXISTS Movies;

CREATE TABLE Movies(
Id INT PRIMARY KEY IDENTITY(1,1),
Name VARCHAR(MAX),
YOR INT,
Plot VARCHAR(MAX),
Poster NVARCHAR(MAX), 
ProducerId INT FOREIGN KEY REFERENCES Producers(Id)
);

INSERT INTO Movies VALUES('Avatar','2009','Jake, who is paraplegic, replaces his twin on the Navi inhabited Pandora for a corporate mission','avatar.jpg',5);
INSERT INTO Movies VALUES('Rocky','1987','Rocky, a small-time boxer, gets a chance to fight heavyweight champion Apollo Creed','rockyH.jpg',3);
INSERT INTO Movies VALUES('Terminal','1996','Viktor Navorski gets stranded at an airport when a war rages in his country','terminal.jpg',4);
INSERT INTO Movies VALUES('Rambo','2004','John Rambo, a former US soldier traumatised by memories of the Vietnam War','rambo.jpg',2);
INSERT INTO Movies VALUES('Rudy','1992','Rudy has always been told that he was too small to play college football','rudy.jpg',5);

SELECT * 
FROM Movies;

DROP TABLE IF EXISTS Reviews;

CREATE TABLE Reviews(
Id INT PRIMARY KEY IDENTITY(1,1),
MovieId INT FOREIGN KEY REFERENCES Movies(Id),
Comment NVARCHAR(MAX)		
);

INSERT INTO Reviews VALUES(1,'good');
INSERT INTO Reviews VALUES(2,'bad');
INSERT INTO Reviews VALUES(3,'good');
INSERT INTO Reviews VALUES(4,'good');
INSERT INTO Reviews VALUES(1,'bad');
INSERT INTO Reviews VALUES(5,'bad');

SELECT * 
FROM Reviews;

DROP TABLE IF EXISTS MovieActorMapping;

CREATE TABLE MovieActorMapping(
Id INT PRIMARY KEY IDENTITY(1,1),
MovieId INT FOREIGN KEY REFERENCES Movies(Id),
ActorId INT FOREIGN KEY REFERENCES Actors(Id)
);

INSERT INTO MovieActorMapping VALUES(1,1);
INSERT INTO MovieActorMapping VALUES(1,3); 
INSERT INTO MovieActorMapping VALUES(1,5);
INSERT INTO MovieActorMapping VALUES(2,6); 
INSERT INTO MovieActorMapping VALUES(2,5);
INSERT INTO MovieActorMapping VALUES(2,4);  
INSERT INTO MovieActorMapping VALUES(2,2);    
INSERT INTO MovieActorMapping VALUES(3,3); 
INSERT INTO MovieActorMapping VALUES(3,2);  
INSERT INTO MovieActorMapping VALUES(4,1);  
INSERT INTO MovieActorMapping VALUES(4,6);    
INSERT INTO MovieActorMapping VALUES(4,3);     
INSERT INTO MovieActorMapping VALUES(5,2); 
INSERT INTO MovieActorMapping VALUES(5,5); 
INSERT INTO MovieActorMapping VALUES(5,3);   

SELECT * 
FROM MovieActorMapping;

DROP TABLE IF EXISTS Genres;

CREATE TABLE Genres(
Id INT PRIMARY KEY IDENTITY(1,1),
Name VARCHAR(MAX)
);

INSERT INTO Genres VALUES('Action');
INSERT INTO Genres VALUES('Drama');
INSERT INTO Genres VALUES('Sports');
INSERT INTO Genres VALUES('Romance');
INSERT INTO Genres VALUES('Horror');
INSERT INTO Genres VALUES('Comedy');
INSERT INTO Genres VALUES('Sci-Fi');
INSERT INTO Genres VALUES('Thriller');
INSERT INTO Genres VALUES('Fiction');


SELECT *
FROM Genres;

DROP TABLE IF EXISTS MovieGenreMapping;

CREATE TABLE MovieGenreMapping(
Id INT PRIMARY KEY IDENTITY(1,1),
MovieId INT FOREIGN KEY REFERENCES Movies(Id),
GenreId INT FOREIGN KEY REFERENCES Genres(Id)
);


INSERT INTO MovieGenreMapping VALUES(1,1);
INSERT INTO MovieGenreMapping VALUES(1,2);
INSERT INTO MovieGenreMapping VALUES(1,3);
INSERT INTO MovieGenreMapping VALUES(1,4);
INSERT INTO MovieGenreMapping VALUES(2,1);
INSERT INTO MovieGenreMapping VALUES(2,2);
INSERT INTO MovieGenreMapping VALUES(2,3);
INSERT INTO MovieGenreMapping VALUES(2,4);
INSERT INTO MovieGenreMapping VALUES(3,1);
INSERT INTO MovieGenreMapping VALUES(3,2);
INSERT INTO MovieGenreMapping VALUES(4,1);
INSERT INTO MovieGenreMapping VALUES(4,2);
INSERT INTO MovieGenreMapping VALUES(4,8);
INSERT INTO MovieGenreMapping VALUES(5,2);
INSERT INTO MovieGenreMapping VALUES(5,3);


SELECT *
FROM MovieGenreMapping;


------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--1) Create Stored Procedure to AddMovie

DROP PROCEDURE IF EXISTS usp_AddMovie;

GO
CREATE PROCEDURE usp_AddMovie 
@Name VARCHAR(MAX), 
@YOR INT,
@Plot VARCHAR(MAX), 
@Poster NVARCHAR(MAX), 
@ProducerId INT,
@ActorIds VARCHAR(MAX), 
@GenreIds VARCHAR(MAX)
AS
BEGIN
   INSERT INTO Movies VALUES(@Name, @YOR, @Plot, @Poster, @ProducerId)
END
BEGIN
   DECLARE @MovieId INT;
   SET @MovieId = SCOPE_IDENTITY();
   INSERT INTO MovieActorMapping
   SELECT @MovieId, CAST(value AS INT) 
   FROM  String_Split(@ActorIds, ',') 
END
BEGIN
   INSERT INTO MovieGenreMapping
   SELECT @MovieId, CAST(value AS INT) 
   FROM  String_Split(@GenreIds, ',') 
END
GO

EXEC usp_AddMovie 
@Name='The Dark Knight', 
@YOR=2008,
@Plot='Batman begin an assault on Gotham organised crime, the mobs hire the Joker, who offers to kill Batman.',
@Poster='', 
@ProducerId=2,
@ActorIds='4,5',
@GenreIds='2,9';
 
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--2) Create Stored Procedure to UpdateMovie

DROP PROCEDURE IF EXISTS usp_UpdateMovie;

GO
CREATE PROCEDURE usp_UpdateMovie
@MovieId INT,
@Name VARCHAR(MAX), 
@YOR INT,
@Plot VARCHAR(MAX), 
@Poster NVARCHAR(MAX), 
@ProducerId INT,
@ActorIds VARCHAR(MAX), 
@GenreIds VARCHAR(MAX)
AS
BEGIN
	UPDATE Movies
	SET 
	Name=@Name, 
	YOR=@YOR,
	Plot=@Plot, 
	Poster=@Poster,
	ProducerId=@ProducerId
	WHERE Id=@MovieId
END
BEGIN
   DELETE FROM MovieActorMapping
   WHERE MovieId=@MovieId
END
BEGIN
   DELETE FROM MovieGenreMapping
   WHERE MovieId=@MovieId
END
BEGIN
   INSERT INTO MovieActorMapping
   SELECT @MovieId, CAST(value AS INT) 
   FROM  String_Split(@ActorIds, ',') 
END
BEGIN
   INSERT INTO MovieGenreMapping
   SELECT @MovieId, CAST(value AS INT) 
   FROM  String_Split(@GenreIds, ',')
END
GO


EXEC usp_UpdateMovie 
@MovieId=7,
@Name='The Dark Knight', 
@YOR=2009,
@Plot='Batman journey', 
@Poster='', 
@ProducerId=5,
@ActorIds='2,3',
@GenreIds='1,8';



------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--3) Create Stored Procedure to DeleteProducer

DROP PROCEDURE IF EXISTS usp_DeleteProducer;

GO
CREATE PROCEDURE usp_DeleteProducer
@ProducerId INT
AS
BEGIN
   SELECT M.Id INTO #RecordsTable
   FROM Movies M
   INNER JOIN Producers P
   ON M.ProducerId=P.Id
   WHERE M.ProducerId=@ProducerId
END
BEGIN 
   DELETE FROM MovieActorMapping
   WHERE MovieId=(SELECT Id FROM  #RecordsTable)
END
BEGIN 
   DELETE FROM MovieGenreMapping
   WHERE MovieId=(SELECT Id FROM  #RecordsTable)
END
BEGIN
    DELETE M
	FROM Movies M
	INNER JOIN Producers P
	ON M.ProducerId=P.Id
	WHERE M.ProducerId=@ProducerId
END
BEGIN
   DELETE FROM Producers
   WHERE Id=@ProducerId
END
GO

EXEC usp_DeleteProducer 
@ProducerId=11



------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--4) Create Stored Procedure to DeleteGenre

DROP PROCEDURE IF EXISTS usp_DeleteGenre;

GO
CREATE PROCEDURE usp_DeleteGenre
@GenreId INT
AS
BEGIN 
   DELETE FROM MovieGenreMapping
   WHERE GenreId=@GenreId
END
BEGIN
    DELETE
	FROM Genres
	WHERE Id=@GenreId
END
GO

EXEC usp_DeleteGenre
@GenreId=12
	

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--5) Create Stored Procedure to DeleteMovie

DROP PROCEDURE IF EXISTS usp_DeleteMovie;

GO
CREATE PROCEDURE usp_DeleteMovie
@MovieId INT
AS
BEGIN 
   DELETE FROM MovieGenreMapping
   WHERE MovieId=@MovieId
END
BEGIN 
   DELETE FROM MovieActorMapping
   WHERE MovieId=@MovieId
END
BEGIN
    DELETE
	FROM Movies
	WHERE Id=@MovieId
END
GO

EXEC usp_DeleteMovie
@MovieId=7
