CREATE DATABASE Batch3;
USE Batch3;
   
CREATE TABLE Employees (
  Id INT PRIMARY KEY,
  Name VARCHAR(30),
  Gender CHAR,
  Address VARCHAR(30)
  );

  INSERT INTO Employees (Id, Name, Gender, Address)
  VALUES(1,'Neeraj','M','BLR');

  INSERT INTO Employees (Id, Name, Gender, Address)
  VALUES(2,'Govind','M','DEL');

  INSERT INTO Employees (Id, Name, Gender, Address)
  VALUES(3,'Amit','M','KOL');

  SELECT * FROM Employees;

   SELECT Name FROM Employees WHERE Id=1;

  SELECT Name FROM Employees WHERE Name LIKE 'N%';


  /*

  Student(Id, Name, BranchId)

Student(Id, Name, BranchName, HOD)

Student(Id, Name, BranchId)
1 neeraj 2
2 govind 4

Branch(Id, Name)
1  compsci          1
2  communications   2
3  chemistrysci     3
4  electrical       4

Hod()
1  arvind
2  gopal
3  sandeep
4  prem

*/

   
   
