
CREATE DATABASE SQLAssignment;
USE SQLAssignment;

--SQL DATE: YYYY-MM-DD 

CREATE TABLE Actors(
Id INT PRIMARY KEY IDENTITY(1,1),
Name VARCHAR(30),
Gender VARCHAR(10),
DOB DATE
);

INSERT INTO Actors VALUES('Mila Kunis','Female','1986/11/14');
INSERT INTO Actors VALUES('Robert DeNiro','Male','1957/07/10');
INSERT INTO Actors VALUES('George Michael','Male','1978/11/23');
INSERT INTO Actors VALUES('Mike Scott','Male','1969/08/06');
INSERT INTO Actors VALUES('Pam Halpert','Female','1996/09/26');
INSERT INTO Actors VALUES('Dame Judi Dench','Female','1947/04/05');


SELECT * 
FROM Actors;


CREATE TABLE Producers(
Id INT PRIMARY KEY IDENTITY(1,1),
Name VARCHAR(30),
Company VARCHAR(30),
CompanyEstDate DATE
);

INSERT INTO Producers VALUES('Arjun','Fox','1998/05/14');
INSERT INTO Producers VALUES('Arun','Bull','2004/09/11');
INSERT INTO Producers VALUES('Tom','Hanks','1987/11/03');
INSERT INTO Producers VALUES('Zeshan','Male','1996/11/14');
INSERT INTO Producers VALUES('Nicole','Team Coco','1992/09/26');

SELECT * 
FROM Producers;

CREATE TABLE Movies(
Id INT PRIMARY KEY IDENTITY(1,1),
Name VARCHAR(30),
Language VARCHAR(30),
ProducerId INT FOREIGN KEY REFERENCES Producers(Id),
Profit INT
);

INSERT INTO Movies VALUES('Rocky','English',1,10000);
INSERT INTO Movies VALUES('Rocky','Hindi',3,3000);
INSERT INTO Movies VALUES('Terminal','English',4,300000);
INSERT INTO Movies VALUES('Rambo','Hindi',2,93000);
INSERT INTO Movies VALUES('Rudy','English',5,9600);

SELECT *
FROM Movies;


CREATE TABLE MovieActorMapping(
Id INT PRIMARY KEY IDENTITY(1,1),
MovieId INT FOREIGN KEY REFERENCES Movies(Id),
ActorId INT FOREIGN KEY REFERENCES Actors(Id)
);

INSERT INTO MovieActorMapping VALUES(1,1);
INSERT INTO MovieActorMapping VALUES(1,3); 
INSERT INTO MovieActorMapping VALUES(1,5);
INSERT INTO MovieActorMapping VALUES(2,6); 
INSERT INTO MovieActorMapping VALUES(2,5);
INSERT INTO MovieActorMapping VALUES(2,4);  
INSERT INTO MovieActorMapping VALUES(2,2);    
INSERT INTO MovieActorMapping VALUES(3,3); 
INSERT INTO MovieActorMapping VALUES(3,2);  
INSERT INTO MovieActorMapping VALUES(4,1);  
INSERT INTO MovieActorMapping VALUES(4,6);    
INSERT INTO MovieActorMapping VALUES(4,3);     
INSERT INTO MovieActorMapping VALUES(5,2); 
INSERT INTO MovieActorMapping VALUES(5,5); 
INSERT INTO MovieActorMapping VALUES(5,3);   

SELECT * 
FROM MovieActorMapping;


---------------------------------------
------------QUERIES--------------------

----1) Update Profit of all the movies by +1000 where producer name contains 'run'

UPDATE Movies 
SET Profit=Profit+1000 
WHERE ProducerId IN ( SELECT Id
                      FROM Producers											
					  WHERE Name LIKE '%run%'
					 );
SELECT * 
FROM MOVIES;

----OR

UPDATE M
SET M.Profit=M.Profit+1000
FROM Movies M
INNER JOIN Producers P
ON M.ProducerId=P.Id 
WHERE P.Name LIKE '%run%'; 

SELECT * 
FROM MOVIES;


----2) Find duplicate movies having the same name and their count

SELECT Name, COUNT(M.Name) AS [Count]
FROM Movies M
GROUP BY Name
HAVING COUNT(M.Name) > 1;


----3) Find the oldest actor/actress for each movie

SELECT M.Id, 
       M.Name AS [Movie Name], 
	   M.Language, 
	   (SELECT  AA.Name FROM Actors AA WHERE DOB=MIN(A.DOB)) AS [Oldest Actor / Actress], 
	   (SELECT  AA.DOB FROM Actors AA WHERE DOB=MIN(A.DOB)) AS [DOB]
FROM MovieActorMapping AM
INNER JOIN Movies M
ON M.Id=AM.MovieId
INNER JOIN Actors A
ON A.Id=AM.ActorId
GROUP BY M.Id, M.Name, M.Language;

----OR

SELECT M.Id, 
       M.Name AS [Movie Name], 
	   M.Language, 
	   (SELECT  AA.Name FROM Actors AA WHERE DOB=MIN(A.DOB)) AS [Oldest Actor / Actress], 
	   (SELECT MIN(A.DOB)) AS [DOB]
FROM MovieActorMapping AM
INNER JOIN Movies M
ON M.Id=AM.MovieId
INNER JOIN Actors A
ON A.Id=AM.ActorId
GROUP BY M.Id, M.Name, M.Language;


----4) List of producers who have not worked with actor X

SELECT A.Name AS [Actor Name], 
       P.Name AS [Producer Name] 
FROM Actors A,Producers P
INNER JOIN Movies M
ON M.ProducerId=P.Id
WHERE A.Id IN ( SELECT AA.Id FROM ACTORS AA)
EXCEPT
SELECT A.Name AS [Actor Name], 
       P.Name AS [Producer Name] 
FROM Producers P
INNER JOIN Movies M
ON M.ProducerId=P.Id
INNER JOIN MovieActorMapping AM
ON AM.MovieId=M.Id
INNER JOIN Actors A 
ON A.Id=AM.ActorId 


--LIST OF PRODUCERS WHO HAVE WORKED WITH ACTOR X
--Dame Judi Dench -> Arun, Tom
--George Michael -> Arjun, Arun ,Nicole, Zeshan
--Mike Scott -> Tom
--Mila kunis-> Arjun, Arun
--Pam Halpert -> Arjun, Nicole, Tom 
--Rober DeNiro -> Nicole, Tom, Zeshan


----5) List of pair of actors who have worked together in more than 2 movies

SELECT A.Name AS [First Actor Name],
	   A2.Name AS [Second Actor Name],    
	   COUNT(*) AS [Movie Count]   
FROM MovieActorMapping AM 
INNER JOIN MovieActorMapping AM2
ON AM.Movieid = AM2.Movieid	
INNER JOIN Actors A
ON A.Id=AM.ActorId
INNER  JOIN Actors A2
ON A2.Id=AM2.ActorId 
WHERE AM.ActorId < AM2.ActorId
GROUP BY A.Name, A2.Name, AM.ActorId, AM2.ActorId
HAVING COUNT(*) >= 2
ORDER BY  [Movie Count] DESC, AM.ActorId ASC,  AM2.ActorId ASC ;  



----6) Add non-clustered index on profit column of movies table

CREATE NONCLUSTERED INDEX IX_Movies_Profit_ASC
ON Movies(Profit ASC)

--SET STATISTICS TIME ON
--SET STATISTICS IO ON

SELECT * FROM MOVIES
WHERE Profit=4700;

SELECT * FROM [dbo].[Movies]
WHERE Profit=4700;


--DROP INDEX [Non-clustered-index-name] ON [dbo].[Table-name];   --dbo : database owner  



----7) Create stored procedure to return list of actors for given movie id

GO
CREATE PROCEDURE ListOfActors @MovieId INT
AS
SELECT A.Name, AM.MovieId
FROM Actors A
INNER JOIN MovieActorMapping AM
ON A.Id=AM.ActorId
WHERE AM.MovieId=@MovieId
GO

EXEC ListOfActors @MovieId=3;


----8) Create a function to return age for given date of birth

GO
CREATE FUNCTION CalculateAge (@DOB DATE)
RETURNS INT
AS 
BEGIN
   RETURN DATEDIFF(year,@DOB,GETDATE()) 
END
Go

SELECT dbo.CalculateAge('1999/05/28');

   

----9) Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated) 

GO
CREATE PROCEDURE IncreaseProfitForGivenMovieIds @MovieIds VARCHAR(30)
AS
UPDATE Movies SET Profit=Profit+100 
WHERE Id IN  ( 
                SELECT Id
                FROM STRING_SPLIT( @MovieIds,',')										
		      )
GO

EXEC IncreaseProfitForGivenMovieIds @MovieIds='1,2,3';

SELECT * 
FROM Movies;


