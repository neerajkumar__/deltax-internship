
CREATE DATABASE HandsOnSession;

USE HandsOnSession;

--SET IDENTITY_INSERT [Table_Name] ON;

CREATE TABLE Classes(
  Id INT PRIMARY KEY  IDENTITY(1,1),
  Name VARCHAR(10),
  Section CHAR,
  Number INT
  );

INSERT INTO Classes VALUES ('IX','A',201);
INSERT INTO Classes VALUES ('IX','B',202);
INSERT INTO Classes VALUES ('X','A',203);


SELECT * 
FROM Classes;

CREATE TABLE Teachers(
  Id INT PRIMARY KEY IDENTITY(1,1),
  Name VARCHAR(20),
  DOB DATE,
  Gender VARCHAR(10)
  );


INSERT INTO Teachers VALUES ('Lisa Kudrow','1985/06/08','Female');
INSERT INTO Teachers VALUES ('Monica Bing','1982/03/06','Female');
INSERT INTO Teachers VALUES ('Chandler Bing','1978/12/17','Male');
INSERT INTO Teachers VALUES ('Ross Geller','1993/01/26','Male');

SELECT * 
FROM Teachers;

CREATE TABLE Students(
  Id INT PRIMARY KEY IDENTITY(1,1),
  Name VARCHAR(30),
  DOB DATE,
  Gender VARCHAR(10),
  ClassId INT FOREIGN KEY REFERENCES Classes(Id)
  );


INSERT INTO Students VALUES ('Scotty Loman','2006/01/31','Male',1);
INSERT INTO Students VALUES ('Adam Scott','2005/06/01','Male',1);
INSERT INTO Students VALUES ('Natosha Beckles','2005/01/23','Female',2);
INSERT INTO Students VALUES ('Lilly Page','2006/11/26','Female',2);
INSERT INTO Students VALUES ('John Freeman','2006/06/14','Male',2);
INSERT INTO Students VALUES ('Morgan Scott','2005/05/18','Male',3);
INSERT INTO Students VALUES ('Codi Gass','2005/12/24','Female',3);
INSERT INTO Students VALUES ('Nick Roll','2005/12/24','Male',3);
INSERT INTO Students VALUES ('Dave Grohl','2005/02/12','Male',3);

SELECT * 
FROM Students;

CREATE TABLE TeacherClassMapping(
  Id INT PRIMARY KEY IDENTITY(1,1),
  TeacherId INT FOREIGN KEY REFERENCES Teachers(Id),
  ClassId INT FOREIGN KEY REFERENCES Classes(Id)
  );

INSERT INTO TeacherClassMapping VALUES (1,1);
INSERT INTO TeacherClassMapping VALUES (1,2);
INSERT INTO TeacherClassMapping VALUES (2,2);
INSERT INTO TeacherClassMapping VALUES (2,3);
INSERT INTO TeacherClassMapping VALUES (3,3);
INSERT INTO TeacherClassMapping VALUES (3,1);

SELECT * 
FROM TeacherClassMapping;

--------------------------------------------------------------------
-------------------------QUERIES------------------------------------

----1) Find list of male students

SELECT * 
FROM Students S
WHERE S.Gender='Male';


----2) Find list of Student older than 2005/01/01

SELECT * 
FROM Students S
WHERE S.DOB < '2005-01-01' ;


----3) Youngest Student in School

SELECT * 
FROM Students S
WHERE S.DOB = (
                 SELECT MIN(DOB) FROM Students
			   );


----4) Find Student distinct Birthdays

SELECT DISTINCT(DOB) AS [Distinct DOB] 
FROM Students;


--5) No. of Students in each class

SELECT C.Name AS [Class], COUNT(S.Id) AS [No. Of Students] 
FROM Classes C, Students S
WHERE C.Id=S.ClassId
GROUP BY C.Name;

----OR

SELECT C.Name AS [Class], COUNT(S.Id) AS [No. Of Students] 
FROM Classes C
INNER JOIN Students S
ON C.Id=S.ClassId
GROUP BY C.Name;


----6) No. of Students in each section

SELECT C.Section AS [Section], COUNT(S.Id) AS [No. Of Students] 
FROM Classes C, Students S
WHERE C.Id=S.ClassId
GROUP BY C.Section;

----OR

SELECT C.Section AS [Section], COUNT(S.Id) AS [No. Of Students]
FROM Classes C
INNER JOIN Students S
ON C.Id=S.ClassId
GROUP BY C.Section;


----7) No. of Classes taught by teacher

SELECT T.Name AS [Teacher Name], COUNT(M.ClassId) AS [No. Of Classes Taught]
FROM Teachers T, TeacherClassMapping M
WHERE T.Id=M.TeacherId
GROUP BY T.Name;

----OR

SELECT T.Name AS [Teacher Name], COUNT(M.ClassId) AS [No. Of Classes Taught]
FROM Teachers T
INNER JOIN TeacherClassMapping M
ON T.Id=M.TeacherId
GROUP BY T.Name;


----8) List of Teachers teaching Class X

SELECT T.Name AS [Teachers teaching Class X]
FROM Teachers T, TeacherClassMapping M
WHERE T.Id=M.TeacherId
AND M.ClassId=3;

----OR

SELECT T.Name AS [Teachers teaching Class X]
FROM Teachers T, TeacherClassMapping M
WHERE T.Id=M.TeacherId
AND M.ClassId IN ( SELECT C.Id FROM Classes C
			         WHERE C.Name='X'
				   );

----OR

SELECT T.Name AS [Teachers teaching Class X]
FROM Teachers T
INNER JOIN TeacherClassMapping M
ON T.Id=M.TeacherId
INNER JOIN Classes C
ON M.ClassID=C.Id AND C.Name='X';


----9) Classes which have more than 2 teachers teaching

SELECT C.Name AS [Classes having more than 2 teachers teaching]
From Classes C, TeacherClassMapping M
WHERE C.Id=M.ClassId
GROUP BY C.Name
HAVING COUNT(C.Name) > 2;

----OR

SELECT C.Name AS [Classes having more than 2 teachers teaching]
From Classes C
INNER JOIN TeacherClassMapping M
ON C.Id=M.ClassId
GROUP BY C.Name
HAVING COUNT(C.Name) >= 2;



----10) List of students being taught by 'Lisa'

SELECT S.Name AS [Students being taught by Lisa]
FROM Students S
WHERE S.ClassId IN (
                      SELECT M.ClassId 
					  FROM TeacherClassMapping M
			          WHERE M.TeacherId=(
					                        SELECT Id 
											FROM  Teachers 
											WHERE Name LIKE 'Lisa%'
										)
			       );

----OR

SELECT S.Name AS [Students being taught by Lisa]
FROM Students S
INNER JOIN TeacherClassMapping M
ON S.ClassId=M.ClassId 
INNER JOIN Teachers T
ON T.Id=M.TeacherId AND  T.Name LIKE 'Lisa%'

