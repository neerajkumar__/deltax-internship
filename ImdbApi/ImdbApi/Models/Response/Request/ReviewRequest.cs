﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Models.Request
{
    public class ReviewRequest
    {
        public int MovieId { get; set; }
        public string Comment { get; set; }

    }
}
