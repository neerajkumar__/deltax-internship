﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Movie Name cannot be null or empty")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Movie YOR cannot be null or empty")]
        public int YOR { get; set; }
        public string Plot { get; set; }
        public string Poster { get; set; }
        public int ProducerId { get; set; }

    }
}
