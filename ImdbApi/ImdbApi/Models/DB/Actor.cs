﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Models
{
    public class Actor
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Actor Name cannot be null or empty")]
        public string Name { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public string Bio { get; set; }

    }
}
