﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Models
{
    public class Review
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "MovieId cannot be null or empty")]
        public int MovieId { get; set; }
        public string Comment { get; set; }

    }
}
