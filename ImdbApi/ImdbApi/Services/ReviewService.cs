﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public class ReviewService : IReviewService
    {


        private readonly IReviewRepository _reviewRepository;
        public ReviewService(IReviewRepository reviewRepository)
        {
            _reviewRepository = reviewRepository;
        }

        public IEnumerable<ReviewResponse> GetNested(int movieId)
        {
            var reviews = _reviewRepository.GetNested(movieId);
            if (reviews is null)
                return null;

            return reviews.Select(a => new ReviewResponse
            {
                Id = a.Id,
                MovieId = a.MovieId,
                Comment = a.Comment
            });

        }

        public void Add(ReviewRequest review)
        {
            var reviewObj = new Review
            {
                MovieId = review.MovieId,
                Comment = review.Comment


            };
            _reviewRepository.Add(reviewObj);
        }


        public void Update(int id, ReviewRequest review)
        {
            var reviewObj = new Review
            {
                Id = id,
                MovieId = review.MovieId,
                Comment = review.Comment

            };
            _reviewRepository.Update(id, reviewObj);

        }

        public void Delete(int id)
        {
            _reviewRepository.Delete(id);
        }

    }
}
