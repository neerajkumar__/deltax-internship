﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public interface IProducerService
    {
        void Add(ProducerRequest producer);  
        IEnumerable<ProducerResponse> Get();  
        void Update(int id, ProducerRequest actor);  
        void Delete(int id); 
        ProducerResponse Get(int id);
    }
}
