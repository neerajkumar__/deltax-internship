﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;
        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;
        }

        public IEnumerable<ProducerResponse> Get()
        {
            var producers = _producerRepository.Get();
            return producers.Select(a => new ProducerResponse
            {
                Id = a.Id,
                Name = a.Name,
                Bio = a.Bio,
                DOB = a.DOB,
                Gender = a.Gender,
            });
        }

        public void Add(ProducerRequest producer)
        {
            var producerObj = new Producer
            {
                Name = producer.Name,
                Gender = producer.Gender,
                DOB = producer.DOB,
                Bio = producer.Bio,

            };
            _producerRepository.Add(producerObj);
        }

        public ProducerResponse Get(int id)
        {
            
            var producers = _producerRepository.Get(id);
            if (producers is null)
                return null;

            return new ProducerResponse
            {
                Id = producers.Id,
                Name = producers.Name,
                Gender = producers.Gender,
                DOB = producers.DOB,
                Bio = producers.Bio,
            };
        }

        public void Update(int id, ProducerRequest producer)
        {
            var producerObj = new Producer
            {
                Id=id,
                Name = producer.Name,
                Gender = producer.Gender,
                DOB = producer.DOB,
                Bio = producer.Bio,

            };
            _producerRepository.Update(id, producerObj);
        }

        public void Delete(int id)
        {
            _producerRepository.Delete(id);
        }

    }
}
