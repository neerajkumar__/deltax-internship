﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;
        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }
        public IEnumerable<ActorResponse> Get()
        {
            var actors = _actorRepository.Get();
            return actors.Select(a => new ActorResponse
            {
                Id = a.Id,
                Name = a.Name,
                Bio = a.Bio,
                DOB = a.DOB,
                Gender = a.Gender,
            });
        }

        public void Add(ActorRequest actor)
        {
            var actorObj = new Actor
            {
                Name = actor.Name,
                Gender = actor.Gender,
                DOB = actor.DOB,
                Bio = actor.Bio,

            };
            _actorRepository.Add(actorObj);
        }

        public ActorResponse Get(int id)
        {
            var actors = _actorRepository.Get(id);
            if (actors is null)
                return null;

            
            return new ActorResponse
            {
                Id = actors.Id,
                Name = actors.Name,
                Gender = actors.Gender,
                DOB = actors.DOB,
                Bio = actors.Bio,
            };
        }

        public void Update(int id, ActorRequest actor)
        {
            var actorObj = new Actor
            {
                Id=id,
                Name = actor.Name,
                Gender = actor.Gender,
                DOB = actor.DOB,
                Bio = actor.Bio,

            };
            _actorRepository.Update(id, actorObj);  
    
        }
        public void Delete(int id)
         {
            _actorRepository.Delete(id);
         }

    }
}
