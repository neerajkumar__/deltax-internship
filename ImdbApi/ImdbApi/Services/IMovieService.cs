﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public interface IMovieService
    {
        void Add(MovieRequest movie);  
        IEnumerable<MovieResponse> Get();  
        void Update(int id, MovieRequest movie);  
        void Delete(int id); 
        MovieResponse Get(int id);
    }
}
