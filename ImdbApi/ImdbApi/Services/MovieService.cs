﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        public MovieService(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }
        public IEnumerable<MovieResponse> Get()
        {
            var movies = _movieRepository.Get();
            return movies.Select(m => new MovieResponse
            {
                Id=m.Id,
                Name=m.Name,
                YOR = m.YOR,
                Plot=m.Plot,
                Poster=m.Poster,
                ProducerId=m.ProducerId
            });
        }

        public void Add(MovieRequest movie)
        {
            var movieObj = new Movie
            {

                Name = movie.Name,
                YOR = movie.YOR,
                Plot = movie.Plot,
                Poster = movie.Poster,
                ProducerId =movie.ProducerId                

            };
            _movieRepository.Add(movieObj);
        }

        public MovieResponse Get(int id)
        {
            var movies = _movieRepository.Get(id);
            if (movies is null)
                return null;

            return new MovieResponse
            {
                Id = movies.Id,
                Name = movies.Name,
                YOR=movies.YOR,
                Plot = movies.Plot,
                Poster = movies.Poster,
                ProducerId = movies.ProducerId
            };
        }

        public void Update(int id, MovieRequest movie)
        {
            var movieObj = new Movie
            {

                Id = id,
                Name = movie.Name,
                YOR = movie.YOR,
                Plot = movie.Plot,
                Poster = movie.Poster,
                ProducerId = movie.ProducerId


            };
            _movieRepository.Update(id, movieObj);
        }

        public void Delete(int id)
        {
            _movieRepository.Delete(id);
        }
       
    }
}
