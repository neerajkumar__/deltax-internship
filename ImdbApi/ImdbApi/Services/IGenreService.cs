﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public interface IGenreService
    {
        void Add(GenreRequest genre);  
        IEnumerable<GenreResponse> Get();  
        void Update(int id, GenreRequest genre);  
        void Delete(int id); 
        GenreResponse Get(int id);
    }
}
