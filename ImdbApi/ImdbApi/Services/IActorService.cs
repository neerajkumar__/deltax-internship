﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public interface IActorService
    {
        void Add(ActorRequest actor); 
        IEnumerable<ActorResponse> Get();  
        void Update(int id, ActorRequest actor); 
        void Delete(int id); 
        ActorResponse Get(int id);
    }
}
