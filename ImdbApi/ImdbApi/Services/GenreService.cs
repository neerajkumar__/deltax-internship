﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;
        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }
        public IEnumerable<GenreResponse> Get()
        {
            var genres = _genreRepository.Get();
            return genres.Select(a => new GenreResponse
            {
                Id = a.Id,
                Name = a.Name,
                
            });
        }
              
        public void Add(GenreRequest genre)
        {
            var genreObj = new Genre
            {
                Name = genre.Name,
                
            };
            _genreRepository.Add(genreObj);
        }

        public GenreResponse Get(int id)
        {
          
            var genres = _genreRepository.Get(id);
            if (genres is null)
                return null;

            return new GenreResponse
            {
                Id = genres.Id,
                Name = genres.Name,
                
            };
        }

        public void Update(int id, GenreRequest genre)
        {
            var genreObj = new Genre
            {
                Id=id,
                Name = genre.Name,

            };
            _genreRepository.Update(id, genreObj);
            
        }

        public void Delete(int id)
         {
            _genreRepository.Delete(id);
         }

    }
}
