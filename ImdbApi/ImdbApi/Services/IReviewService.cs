﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Services
{
    public interface IReviewService
    {
        IEnumerable<ReviewResponse> GetNested(int movieId);
        void Add(ReviewRequest review);
        void Update(int id, ReviewRequest review);
        void Delete(int id);

    }
}
