﻿using Dapper;
using ImdbApi.Models;
using ImdbApi.Models.Request;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Repository
{
    public class GenreRepository  : IGenreRepository
    {
        private readonly Connection _connection;

        public GenreRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }
        
        public IEnumerable<Genre> Get()
        {
            const string sql = @" SELECT *
                                  FROM Genres";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var genres = connection.Query<Genre>(sql);
                return genres;
            }
        }

        public Genre Get(int id)
        {
            const string sql = @"SELECT Id, Name
                           FROM Genres
                           WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirst<Genre>(sql, new { Id = id });
                return query;
            }
           
        }

        public void Add(Genre genre)
        {
            const string sql = @" INSERT INTO Genres VALUES (@Name)";

            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Execute(sql, new
                {
                    genre.Name,

                });
                
            }
        }

        public void Update(int id, Genre genre)
        {
            const string sql = @"UPDATE Genres
                SET 
                Name = @Name
                WHERE ID = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirstOrDefault<Genre>(sql, new {
                    genre.Id,
                    genre.Name
                });

            }
            
        }

        public void Delete(int id)
        {
            const string sql = @"DELETE
                           FROM Genres
                           WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirstOrDefault<Genre>(sql, new { Id = id });
                
            }
        }

    }
 }

