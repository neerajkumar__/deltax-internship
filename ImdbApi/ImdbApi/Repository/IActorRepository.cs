﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Repository
{
    public interface IActorRepository
    {
        IEnumerable<Actor> Get();
        Actor Get(int id);
        void Add(Actor actor);
        void Delete(int id);
        void Update(int id, Actor actor);
    }
}
