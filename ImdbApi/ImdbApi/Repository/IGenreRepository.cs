﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Repository
{
    public interface IGenreRepository
    {
        IEnumerable<Genre> Get();
        Genre Get(int id);
        void Add(Genre genre);
        void Delete(int id);        
        void Update(int id, Genre genre);
    }
}
