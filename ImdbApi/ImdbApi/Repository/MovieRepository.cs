﻿using Dapper;
using ImdbApi.Models;
using ImdbApi.Models.Request;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Repository
{
    public class MovieRepository : IMovieRepository
    {
        private readonly Connection _connection;

        public MovieRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }

        public IEnumerable<Movie> Get()
        {
            const string sql = @" SELECT *
                                  FROM Movies";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var movies = connection.Query<Movie>(sql);
                return movies;
            }

        }

        public Movie Get(int id)
        {
            const string sql = @"SELECT Id, Name, YOR, Plot, Poster, ProducerId
                           FROM Movies
                           WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirst<Movie>(sql, new { Id = id });
                return query;
            }
        }

        public void Add(Movie movie)
        {
            string sql = @" INSERT INTO Movies VALUES (@Name, @YOR, @Plot, @Poster, @ProducerId)";

            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Execute(sql, new
                {
                    movie.Name,
                    movie.YOR,
                    movie.Plot,
                    movie.Poster,
                    movie.ProducerId

                });
            }

        }
        public void Update(int id, Movie movie)
        {
            const string sql = @"UPDATE Movies 
                SET Name = @Name, 
                YOR = @YOR, 
                Plot = @Plot,
                Poster=@Poster,
                ProducerId=@ProducerId
                WHERE ID = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirstOrDefault<Movie>(sql, new { 
                    movie.Id,
                    movie.Name,
                    movie.YOR,
                    movie.Plot,
                    movie.Poster,
                    movie.ProducerId
                });

            }
          
        }

        public void Delete(int id)
        {
                string sql = @"DELETE
                           FROM Movies
                           WHERE Id = @Id";

                using (var connection = new SqlConnection(_connection.DB))
                {
                    var query = connection.QueryFirstOrDefault<Movie>(sql, new { Id = id });
                    
                }
        }
                
    }
}

