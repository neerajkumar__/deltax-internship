﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Repository
{
    public interface IProducerRepository
    {
        IEnumerable<Producer> Get();
        Producer Get(int id);
        void Add(Producer Producer);
        void Delete(int id);
        void Update(int id, Producer producer);
    }
}
