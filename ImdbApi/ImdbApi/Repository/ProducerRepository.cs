﻿using Dapper;
using ImdbApi.Models;
using ImdbApi.Models.Request;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Repository
{
    public class ProducerRepository  : IProducerRepository
    {
        private readonly Connection _connection;

        public ProducerRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }
    
        public IEnumerable<Producer> Get()
        {
            const string sql = @" SELECT *
                                  FROM Producers";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var producers = connection.Query<Producer>(sql);
                return producers;
            }
                       
        }

        public Producer Get(int id)
        {
            const string sql = @"SELECT Id, Name, Gender, DOB, Bio
                           FROM Producers
                           WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirst<Producer>(sql, new { Id = id });
                return query;
            }
            
        }

        public void Add(Producer producer)
        {
            const string sql = @" INSERT INTO Producers VALUES (@Name, @Gender, @DOB, @Bio)";

            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Execute(sql, new
                {
                    producer.Name,
                    producer.Gender,
                    producer.DOB,
                    producer.Bio,

                });
            }

        }

        public void Update(int id, Producer producer)
        {

            const string sql = @"UPDATE Producers 
                SET 
                Name = @Name, 
                Gender=@Gender,
                DOB = @DOB, 
                Bio = @Bio
                WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirstOrDefault<Producer>(sql, new
                {
                    producer.Id,
                    producer.Name,
                    producer.Gender,
                    producer.DOB,
                    producer.Bio
                });
            }
        }

        public void Delete(int id)
        {
            const string sql = @"DELETE
                           FROM Producers
                           WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirstOrDefault<Producer>(sql, new { Id = id });
                
            }
            
        }
             
    }
 }

