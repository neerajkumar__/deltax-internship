﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;

namespace ImdbApi.Repository
{
    public class ActorRepository :  IActorRepository
    {
        private readonly Connection _connection;
        
        public ActorRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }

        public IEnumerable<Actor> Get()
        {
            const string sql = @" SELECT *
                                  FROM Actors";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var actors = connection.Query<Actor>(sql);
                return actors;
            }

        }

        public Actor Get(int id)
        {
            const string sql = @"SELECT Id, Name, Gender, DOB, Bio
                           FROM Actors
                           WHERE Id = @Id";

                using (var connection = new SqlConnection(_connection.DB))
                {
                    var query = connection.QueryFirstOrDefault<Actor>(sql, new { Id = id });
                    return query;
                }
        }

        public void Add(Actor actor)
        {

            const string sql = @" INSERT INTO Actors VALUES (@Name, @Gender, @DOB, @Bio)";

                using (var connection = new SqlConnection(_connection.DB))
                {
                    connection.Execute(sql, new
                    {
                        actor.Name,
                        actor.Gender,
                        actor.DOB,
                        actor.Bio,
                        
                    });
                }            
        }

        public void Update(int id,Actor actor)
        {
            const string sql = @"UPDATE Actors 
                SET 
                Name = @Name, 
                Gender=@Gender,
                DOB = @DOB, 
                Bio = @Bio
                WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirstOrDefault<Actor>(sql, new { 
                    actor.Id,
                    actor.Name,
                    actor.Gender,
                    actor.DOB,
                    actor.Bio
                });

            }
        }

        public void Delete(int id)
        {
            const string sql = @"DELETE
                           FROM Actors
                           WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirstOrDefault<Actor>(sql, new { Id = id });
             
            }            
        }

    }
 }

