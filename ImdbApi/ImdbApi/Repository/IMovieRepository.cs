﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Repository
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> Get();
        Movie Get(int id);
        void Add(Movie movie);
        void Delete(int id);
        void Update(int id, Movie movie);
    }
}
