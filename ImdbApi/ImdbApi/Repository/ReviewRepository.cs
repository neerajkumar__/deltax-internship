﻿using Dapper;
using ImdbApi.Models;
using ImdbApi.Models.Request;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Repository
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly Connection _connection;

        public ReviewRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }

        public IEnumerable<Review> GetNested(int movieId)
        {
            const string sql = @" SELECT *
                                  FROM Reviews
                                  WHERE movieId=@movieId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var reviews = connection.Query<Review>(sql, new { @MovieId = movieId });
                return reviews;
            }

        }

        public void Add(Review review)
        {
            const string sql = @" INSERT INTO Reviews VALUES (@MovieId, @Comment)";

            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Execute(sql, new
                {
                    review.MovieId,
                    review.Comment,

                });
            }
        }

        public void Update(int id, Review review)
        {

            const string sql = @"UPDATE Reviews 
                SET 
                MovieId=@MovieId,
                Comment = @Comment 
                WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirstOrDefault<Models.Review>(sql, new
                {
                    review.Id,
                    review.MovieId,
                    review.Comment
                });
            }
        }

        public void Delete(int id)
        {
            const string sql = @"DELETE
                           FROM Reviews
                           WHERE Id = @Id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var query = connection.QueryFirstOrDefault<Review>(sql, new { Id = id });

            }
        }

    }
}

