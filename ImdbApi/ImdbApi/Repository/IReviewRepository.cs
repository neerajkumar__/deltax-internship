﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Repository
{
    public interface IReviewRepository
    {
        IEnumerable<Review> GetNested(int movieId);
        void Add(Review review);
        void Delete(int id);
        void Update(int id, Review review);
    }
}
