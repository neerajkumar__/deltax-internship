﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Controllers
{
   
    [Route("producers")]
    [ApiController]
    public class ProducerController : ControllerBase
    {

        private readonly IProducerService _producerService;

        public ProducerController(IProducerService producerService)
        {
            _producerService = producerService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var producer = _producerService.Get();
            if (producer.Count() == 0)
            {
                return Ok("No Producer Exists");
            }

            return Ok(producer);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {

            var producer = _producerService.Get(id);
            if (producer is null)
            {
                return NotFound();
            }
            return Ok(producer);
        }

        
        [HttpPost]
        public IActionResult Post([FromBody] ProducerRequest producer)
        {
            _producerService.Add(producer);
            return Ok(_producerService.Get());
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ProducerRequest producer)
        {
            _producerService.Update(id, producer);
            return Ok(_producerService.Get(id));
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _producerService.Delete(id);
            return Ok(_producerService.Get());
        }

    }
}

