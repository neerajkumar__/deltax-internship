﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Controllers
{
    [Route("genres")]
    [ApiController]
    public class GenreController : ControllerBase
    {

        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;   
        }

        [HttpGet]
        public IActionResult Get()
        {
            var genre = _genreService.Get();
            if (genre.Count() == 0)
            {
                return Ok("No Genre Exists");
            }
            return Ok(genre);
        }


        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var genre = _genreService.Get(id);
            if (genre is null)
            {
                return NotFound();
            }

            return Ok(genre);
        }

       
        [HttpPost]
        public IActionResult Post([FromBody] GenreRequest genre)
        {
            _genreService.Add(genre);
            return Ok(_genreService.Get());
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] GenreRequest genre)
        {
          
            _genreService.Update(id, genre);
            return Ok(_genreService.Get(id));
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _genreService.Delete(id);
            return Ok(_genreService.Get());
        }

    }
}

