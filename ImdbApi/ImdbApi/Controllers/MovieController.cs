﻿using Firebase.Storage;
using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ImdbApi.Controllers
{
    [Route("movies")]
    [ApiController]
    public class MovieController : ControllerBase
    {

        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var movie = _movieService.Get();
            if (movie.Count() == 0)
            {
                return Ok("No Movie Exists");
            }

            return Ok(movie);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var movie = _movieService.Get(id);
            if (movie is null)
            {
                return NotFound();
            }
            return Ok(movie);
        }

        [HttpPost]
        public IActionResult Post([FromBody] MovieRequest movie)
        {
            _movieService.Add(movie);
            return Ok(_movieService.Get());
        }
                
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] MovieRequest movie)
        {
            _movieService.Update(id, movie);
            return Ok(_movieService.Get(id));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _movieService.Delete(id);
            return Ok(_movieService.Get());

        }

        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");


            var task = await new FirebaseStorage("imdbapi-c2397.appspot.com")
                  .Child(Guid.NewGuid().ToString() + ".jpg")
                  .PutAsync(file.OpenReadStream());
            return Ok(task);
        }

    }
}

