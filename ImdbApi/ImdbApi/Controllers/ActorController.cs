﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Controllers
{

    [Route("actors")]
    [ApiController]
    public class ActorController : ControllerBase
    {

        private readonly IActorService _actorService;

        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet]
        public IActionResult Get()
        {

            var actor = _actorService.Get();
            if(actor.Count()==0)
            {
                return Ok("No Actor Exists");
            }

            return Ok(actor);
        }


        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var actor = _actorService.Get(id);
            if(actor is null)
            {
                return NotFound();
            }
                     
            return Ok(actor);
        }

        
        [HttpPost]
        public IActionResult Post([FromBody] ActorRequest actor)
        {
            _actorService.Add(actor);
            return Ok(_actorService.Get());
        }



        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ActorRequest actor)
        {
          
            _actorService.Update(id, actor);

            return Ok(_actorService.Get(id));
        }



        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _actorService.Delete(id);
            return Ok(_actorService.Get());
        }

    }
}

