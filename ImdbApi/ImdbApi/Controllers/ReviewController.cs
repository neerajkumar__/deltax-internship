﻿using ImdbApi.Models;
using ImdbApi.Models.Request;
using ImdbApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Controllers
{

    [Route("reviews")]
    [ApiController]
    public class ReviewController : ControllerBase
    {

        private readonly IReviewService _reviewService;

        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }


        //Nested Endpoint, MovieId is coming from route
        [HttpGet]
        [Route("movies/{movieId}")]
        public IActionResult GetNested(int movieId)
        {
            var review = _reviewService.GetNested(movieId);
            if (review.Count() == 0)
            {
                return Ok("No Review Exists For This Movie");
            }

            return Ok(review);
        }


        [HttpPost]
        public IActionResult Post([FromBody] ReviewRequest review)
        {
            _reviewService.Add(review);
            return Ok(_reviewService.GetNested(review.MovieId));
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ReviewRequest review)
        {

            _reviewService.Update(id, review);
            return Ok(_reviewService.GetNested(review.MovieId));
        }


        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _reviewService.Delete(id);
        }

    }
}

