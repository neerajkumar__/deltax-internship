﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImdbApi.Models;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using Moq;

namespace ImdbApi.Test.MockResources
{
    public class ActorMock
    {
    
        public static readonly Mock<IActorRepository> ActorRepoMock = new Mock<IActorRepository>();

        private static readonly List<Actor> ListOfActors = new List<Actor>
        {
            new Actor       
            {
                Id = 1,
                Name = "Mock Actor 1",
                Gender = "Female",
                DOB = Convert.ToDateTime("1986/11/14"),
                Bio = "Bio1"

            },
            new Actor
            {
                Id = 2,
                Name = "Mock Actor 2",
                Gender = "Female",
                DOB = Convert.ToDateTime("1957/07/10"),
                Bio = "Bio2"

            },
            new Actor
            {
                Id=3,
                Name="Mock Actor 3",
                Gender = "Male",
                DOB = Convert.ToDateTime("1959/05/11"),
                Bio = "Bio3"
            }
        };

        public static void MockGet()
        {
            ActorRepoMock.Setup(x => x.Get()).Returns(ListOfActors);
            ActorRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListOfActors.Where(x => x.Id == id).SingleOrDefault());

        }

        public static void MockAdd()
        {
            ActorRepoMock.Setup(x => x.Add(It.IsAny<Actor>())).Callback(new Action<Actor>(actor =>
            {
                 var id = ListOfActors.Last().Id;
                var nextid = id + 1;
                actor.Id = nextid;
                ListOfActors.Add(actor);
            }));
        }

        public static void MockDelete()
        {
            ActorRepoMock.Setup(x => x.Delete(It.IsAny<int>())).Callback((int id) => { ListOfActors.RemoveAll((x) => x.Id == id);});
        }

        public static void MockUpdate()
        {
            ActorRepoMock.Setup(x => x.Update(It.IsAny<int>(), It.IsAny<Actor>())).Callback<int, Actor>((id, actor) => 
             {
                  ListOfActors.RemoveAll(x => x.Id == actor.Id);    
                  actor.Id = id;
                  ListOfActors.Add(actor);

              });
        }

    }
}
