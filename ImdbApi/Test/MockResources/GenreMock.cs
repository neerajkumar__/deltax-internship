﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImdbApi.Models;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using Moq;

namespace ImdbApi.Test.MockResources
{
    public class GenreMock
    {

        public static readonly Mock<IGenreRepository> GenreRepoMock = new Mock<IGenreRepository>();

        private static readonly List<Genre> ListOfGenres = new List<Genre>
        {
            new Genre
            {
                Id = 1,
                Name = "Mock Genre 1",
            },
            new Genre
            {
                Id = 2,
                Name = "Mock Genre 2",
               
            },
            new Genre
            {
                Id=3,
                Name="Mock Genre 3",
              
            }
        };
    
        public static void MockGet()
        {
            GenreRepoMock.Setup(x => x.Get()).Returns(ListOfGenres);
            GenreRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListOfGenres.Where(x => x.Id == id).SingleOrDefault());

        }

        public static void MockAdd()
        {
            GenreRepoMock.Setup(x => x.Add(It.IsAny<Genre>())).Callback(new Action<Genre>(genre =>
            {
                var id = ListOfGenres.Last().Id;
                var nextid = id + 1;
                genre.Id = nextid;
                ListOfGenres.Add(genre);
            }));
        }

        public static void MockDelete()
        {
            GenreRepoMock.Setup(x => x.Delete(It.IsAny<int>())).Callback((int id) => { ListOfGenres.RemoveAll((x) => x.Id == id);});
        }

        public static void MockUpdate()
        {
            GenreRepoMock.Setup(x => x.Update(It.IsAny<int>(), It.IsAny<Genre>())).Callback<int, Genre>((id, genre) =>
            {
                ListOfGenres.RemoveAll(x => x.Id == genre.Id);
                genre.Id = id;
                ListOfGenres.Add(genre);

            });
        }

        
    }
} 