﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImdbApi.Models;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using Moq;

namespace ImdbApi.Test.MockResources
{
    public class ProducerMock
    {
    
        public static readonly Mock<IProducerRepository> ProducerRepoMock = new Mock<IProducerRepository>();

        private static readonly List<Producer> ListOfProducers = new List<Producer>
        {
            new Producer
            {
                Id = 1,
                Name = "Mock Producer 1",
                Gender = "Female",
                DOB = Convert.ToDateTime("1986/11/14"),
                Bio = "Bio1"

            },
            new Producer
            {
                Id = 2,
                Name = "Mock Producer 2",
                Gender = "Female",
                DOB = Convert.ToDateTime("1957/07/10"),
                Bio = "Bio2"

            },
            new Producer
            {
                Id=3,
                Name="Mock Producer 3",
                Gender = "Male",
                DOB = Convert.ToDateTime("1959/05/11"),
                Bio = "Bio3"
            }
        };

        public static void MockGet()
        {
            ProducerRepoMock.Setup(x => x.Get()).Returns(ListOfProducers);
            ProducerRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListOfProducers.Where(x => x.Id == id).SingleOrDefault());
        }

        public static void MockAdd()
        {
            ProducerRepoMock.Setup(x => x.Add(It.IsAny<Producer>())).Callback(new Action<Producer>(producer =>
            {
                var id = ListOfProducers.Last().Id;
                var nextid = id  + 1;
                producer.Id = nextid;
                ListOfProducers.Add(producer);
            }));

        }

        public static void MockDelete()
        {
            ProducerRepoMock.Setup(x => x.Delete(It.IsAny<int>())).Callback((int id) => { ListOfProducers.RemoveAll((x) => x.Id == id); });
        }


        public static void MockUpdate()
        {
            ProducerRepoMock.Setup(x => x.Update(It.IsAny<int>(), It.IsAny<Producer>())).Callback<int, Producer>((id, producer) =>
            {
                ListOfProducers.RemoveAll(x => x.Id == producer.Id);
                producer.Id = id;
                ListOfProducers.Add(producer);

            });
        }

    }
 }