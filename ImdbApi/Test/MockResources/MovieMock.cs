﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImdbApi.Models;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using Moq;

namespace ImdbApi.Test.MockResources
{
    public class MovieMock
    {

        public static readonly Mock<IMovieRepository> MovieRepoMock = new Mock<IMovieRepository>();

        private static readonly List<Movie> ListOfMovies = new List<Movie>
        {

            new Movie
            {
                Id = 1,
                Name = "Mock Movie 1",
                YOR = 2020,
                Plot = "its mock movie 1",
                Poster = "movie1.jpg",
                ProducerId = 1
             },
            new Movie
            {
                Id = 2,
                Name = "Mock Movie 2",
                YOR = 2021,
                Plot = "its mock movie 2",
                Poster = "movie2.jpg",
                ProducerId = 2
            },
            new Movie
            {
                Id = 3,
                Name = "Mock Movie 3",
                YOR = 2019,
                Plot = "its mock movie 3",
                Poster = "movie3.jpg",
                ProducerId = 3
            }
        };

        public static void MockGet()
        {
            MovieRepoMock.Setup(x => x.Get()).Returns(ListOfMovies);
            MovieRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListOfMovies.Where(x => x.Id == id).SingleOrDefault());

        }

        public static void MockAdd()
        {
            MovieRepoMock.Setup(x => x.Add(It.IsAny<Movie>())).Callback(new Action<Movie>(movie =>
            {
                var id = ListOfMovies.Last().Id;
                var nextid = id + 1;
                movie.Id = nextid;
                ListOfMovies.Add(movie);
            }));

        }

        public static void MockDelete()
        {
            MovieRepoMock.Setup(x => x.Delete(It.IsAny<int>())).Callback((int id) => { ListOfMovies.RemoveAll((x) => x.Id == id); });
        }

        public static void MockUpdate()
        {
            MovieRepoMock.Setup(x => x.Update(It.IsAny<int>(), It.IsAny<Movie>())).Callback<int, Movie>((id, movie) =>
            {
                ListOfMovies.RemoveAll(x => x.Id == movie.Id);
                movie.Id = id;
                ListOfMovies.Add(movie);

            });
        }

    }
}

