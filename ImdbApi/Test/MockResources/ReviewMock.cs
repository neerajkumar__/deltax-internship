﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImdbApi.Models;
using ImdbApi.Models.Response;
using ImdbApi.Repository;
using Moq;

namespace ImdbApi.Test.MockResources
{
    public class ReviewMock
    {
  
        public static readonly Mock<IReviewRepository> ReviewRepoMock = new Mock<IReviewRepository>();

        private static readonly List<Review> ListOfReviews = new List<Review>
        {

            new Review   
            {
                Id=1,
                MovieId = 1,
                Comment="good"
            },
            new Review
            {
                Id=2,
                MovieId = 2,
                Comment="bad"
            },
            new Review  
            {
                Id=3,
                MovieId=3,
                Comment="good"
            }
        };


        public static IEnumerable<Review>listreviews(int id)    
        {
            if (ListOfReviews.Any(x => x.MovieId == id))
            {
                var check = ListOfReviews.Where(x => x.MovieId == id);
                return check;
            }
            else
                return null;
        }
        public static void MockGet()
        {
            ReviewRepoMock.Setup(x => x.GetNested(It.IsAny<int>())).Returns((int movieId) => listreviews(movieId));
        }

        public static void MockAdd()
        {
            ReviewRepoMock.Setup(x => x.Add(It.IsAny<Review>())).Callback(new Action<Review>(review =>
            {
                var id = ListOfReviews.Last().Id;
                var nextid = id  + 1;
                review.Id = nextid;
                ListOfReviews.Add(review);
            }));
        }

        public static void MockDelete()
        {
            ReviewRepoMock.Setup(x => x.Delete(It.IsAny<int>())).Callback((int id) => { ListOfReviews.RemoveAll((x) => x.Id == id);});
        }

        public static void MockUpdate()
        {
            ReviewRepoMock.Setup(x => x.Update(It.IsAny<int>(), It.IsAny<Review>())).Callback<int, Review>((id, review) => 
             {
                 ListOfReviews.RemoveAll(x => x.Id == review.Id);    
                 review.Id = id;
                 ListOfReviews.Add(review);

              });
        }

    }
}
