﻿using ImdbApi.Test.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;

namespace ImdbApi.Test.StepFiles
{
    [Scope(Feature = "Review Resource")]
    [Binding]
    public class ReviewSteps : BaseSteps
    {
        public ReviewSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(_ => ReviewMock.ReviewRepoMock.Object);
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            ReviewMock.MockGet();
            ReviewMock.MockDelete();
            ReviewMock.MockAdd();
            ReviewMock.MockUpdate();
        }
    }
}