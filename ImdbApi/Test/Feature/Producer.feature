﻿Feature: Producer Resource


    Scenario: 1.Add Producer
	Given I am a client
	When I am making a post request to '/producers' with the following Data '{"name":"Mock Producer 4","gender":"Male","dob":"2020-06-16T00:00:00","bio":"Bio4"}'
	Then response code must be '200'
	
	Scenario: 2.Update Producer
	Given I am a client
	When I make PUT Request '/producers/4' with the following Data with the following Data '{"name":"Mock Producer 4","gender":"Male","dob":"2020-06-16T00:00:00","bio":"Bio41"}'
	Then response code must be '200'
	
	Scenario: 2.Delete Producer By Id	
	Given I am a client
	When I make Delete Request '/producers/4'
	Then response code must be '200'
	
	Scenario: 3.Get Producer By Id
	Given I am a client
	When I make GET Request '/producers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Mock Producer 1","gender":"Female","dob":"1986-11-14T00:00:00","bio":"Bio1"}'  
		
    Scenario: 4.Get All Producers
	Given I am a client
	When I make GET Request '/producers'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Mock Producer 1","gender":"Female","dob":"1986-11-14T00:00:00","bio":"Bio1"},{"id":2,"name":"Mock Producer 2","gender":"Female","dob":"1957-07-10T00:00:00","bio":"Bio2"},{"id":3,"name":"Mock Producer 3","gender":"Male","dob":"1959-05-11T00:00:00","bio":"Bio3"}]'  
  

