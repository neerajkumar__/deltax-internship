﻿Feature: Genre Resource
 	
    Scenario: 1.Add Genre
	Given I am a client
	When I am making a post request to '/genres' with the following Data '{"name":"Mock Genre 4"}'
	Then response code must be '200'
	
	Scenario: 2.Update Genre
	Given I am a client
	When I make PUT Request '/genres/4' with the following Data with the following Data '{"name":"Mock Genre 41"}'
	Then response code must be '200'
	
	Scenario: 3.Delete Genre By Id
	Given I am a client
	When I make Delete Request '/genres/4'
	Then response code must be '200'
	
	Scenario: 4.Get Genre By Id
	Given I am a client
	When I make GET Request '/genres/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Mock Genre 1"}'  

	Scenario: 5.Get All Genres
	Given I am a client
	When I make GET Request '/genres'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Mock Genre 1"},{"id":2,"name":"Mock Genre 2"},{"id":3,"name":"Mock Genre 3"},{"id":4,"name":"Mock Genre 4"}]'

	