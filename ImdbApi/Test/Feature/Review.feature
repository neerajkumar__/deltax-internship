﻿Feature: Review Resource

    Scenario: 1.Add Review
	Given I am a client
	When I am making a post request to '/reviews' with the following Data '{"id":4,"movieId":4,"comment":"good"}'
	Then response code must be '200'
	
	Scenario: 2.Update Review
	Given I am a client
	When I make PUT Request '/reviews/4' with the following Data with the following Data '{"movieId":4,"comment":"bad"}'
	Then response code must be '200'
	
	
	Scenario: 3.Delete Review By Id
	Given I am a client
	When I make Delete Request '/reviews/4'
	Then response code must be '200'
	

	Scenario: 4.Get Review By MovieId
	Given I am a client
	When I make GET Request '/reviews/movies/1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"movieId":1,"comment":"good"}]'  


	