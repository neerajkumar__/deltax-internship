﻿Feature: Actor Resource

     
    Scenario: 1.Add Actor
	Given I am a client
	When I am making a post request to '/actors' with the following Data '{"name":"Mock Actor 4","gender":"Male","dob":"2020-06-16T00:00:00","bio":"Bio4"}'
	Then response code must be '200'
	
	
	Scenario: 2.Update Actor
	Given I am a client
	When I make PUT Request '/actors/4' with the following Data with the following Data '{"name":"Mock Actor 4","gender":"Male","dob":"2020-06-16T00:00:00","bio":"Bio41"}'
	Then response code must be '200'
		 

	Scenario: 3.Delete Actor By Id
	Given I am a client
	When I make Delete Request '/actors/4'
	Then response code must be '200'
	
	Scenario: 4.Get Actor By Id
	Given I am a client
	When I make GET Request '/actors/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Mock Actor 1","gender":"Female","dob":"1986-11-14T00:00:00","bio":"Bio1"}'  

	
	
	Scenario: 5.Get All Actors
	Given I am a client
	When I make GET Request '/actors'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Mock Actor 1","gender":"Female","dob":"1986-11-14T00:00:00","bio":"Bio1"},{"id":2,"name":"Mock Actor 2","gender":"Female","dob":"1957-07-10T00:00:00","bio":"Bio2"},{"id":3,"name":"Mock Actor 3","gender":"Male","dob":"1959-05-11T00:00:00","bio":"Bio3"}]'
	