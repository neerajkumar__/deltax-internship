﻿Feature: Movie Resource
 
	Scenario: 1.Add Movie
	Given I am a client
	When I am making a post request to '/movies' with the following Data '{"name":"Mock Movie 4","yor":2018,"plot":"its mock movie 4","poster":"movie4.jpg","producerId":4}'
	Then response code must be '200'

	Scenario: 2.Update Movie
	Given I am a client
	When I make PUT Request '/movies/4' with the following Data with the following Data '{"name":"Mock Movie 4","yor":2018,"plot":"its mock movie 4","poster":"movie4.jpg","producerId":41}'
	Then response code must be '200'

	Scenario: 3.Delete Movie By Id
	Given I am a client
	When I make Delete Request '/movies/4' 
	Then response code must be '200'
	

	Scenario: 4.Get Movie By Id
	Given I am a client
	When I make GET Request '/movies/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Mock Movie 1","yor":2020,"plot":"its mock movie 1","poster":"movie1.jpg","producerId":1}'  
	
	Scenario: 5.Get All Movies
	Given I am a client
	When I make GET Request '/movies'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Mock Movie 1","yor":2020,"plot":"its mock movie 1","poster":"movie1.jpg","producerId":1},{"id":2,"name":"Mock Movie 2","yor":2021,"plot":"its mock movie 2","poster":"movie2.jpg","producerId":2},{"id":3,"name":"Mock Movie 3","yor":2019,"plot":"its mock movie 3","poster":"movie3.jpg","producerId":3}]'  
  
    